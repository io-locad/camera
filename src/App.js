import React from 'react';
import './App.css';
import Select from 'react-select';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts';
import { BarChart, Bar, ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
// import { PieChart, Pie, Sector } from 'recharts';
import { Legend } from 'recharts';
// import { PieChart } from 'react-minimal-pie-chart';
// import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, } from 'recharts';

// import { ProgressBar } from '@react-spectrum/progress'; 
// import { Flex, ProgressBar } from '@adobe/react-spectrum';

// import FusionCharts from 'fusioncharts';
// import charts from 'fusioncharts/fusioncharts.charts';
// import ReactFusioncharts from 'react-fusioncharts';
import { CircleProgress } from 'react-gradient-progress';
import { Line } from 'rc-progress';

// charts(FusionCharts);  

// FUSION START 

// FUSION END
// COMPANY LIST
const Company = [
  { label: "Alpha" },
  { label: "Boing" },
  { label: "Camera" },
  { label: "LOCAD"}
];

const Details = [
  { label: "Emotions" },
  { label: "Facial Attributes" },
  { label: "Sequence" },
  { label: "Clothing & Other Details"}
];


const Gen = [
  { label: "Male"},
  {label: "Female"}
];

const Age = [
  { label: "12-18" },
  { label: "18-28" },
  { label: "29-40"},
  {label: "41-70"}
];

// GRAPH DATA
const data = [
  { x: 100, y: 200, z: 200 },
  { x: 120, y: 100, z: 260 },
  { x: 170, y: 300, z: 400 },
  { x: 140, y: 250, z: 280 },
  { x: 150, y: 400, z: 500 },
  { x: 110, y: 280, z: 200 },
  { x: 100, y: 300, z: 200 },
  { x: 120, y: 200, z: 260 },
  { x: 170, y: 100, z: 400 },
  { x: 140, y: 150, z: 280 },
  { x: 150, y: 200, z: 500 },
  { x: 110, y: 120, z: 200 },
  { x: 10, y: 200, z: 200 },
  { x: 20, y: 100, z: 260 },
  { x: 70, y: 300, z: 400 },
  { x: 40, y: 250, z: 280 },
  { x: 50, y: 400, z: 500 },
  { x: 24, y:280, z: 200 },
  { x: 10, y: 20, z: 200 },
  { x: 20, y: 10, z: 260 },
  { x: 3, y: 30, z: 400 },
  { x: 140, y: 20, z: 280 },
  { x: 150, y: 20, z: 500 },
  { x: 110, y: 80, z: 200 },
];


// const data3 = [
//   {
//     name: '18-24', uv: 31.47, pv: 2400, fill: '#8884d8',
//   },
//   {
//     name: '25-29', uv: 26.69, pv: 4567, fill: '#83a6ed',
//   },
//   {
//     name: '30-34', uv: 15.69, pv: 1398, fill: '#8dd1e1',
//   },
//   {
//     name: '35-39', uv: 8.22, pv: 9800, fill: '#82ca9d',
//   },
//   {
//     name: '40-49', uv: 8.63, pv: 3908, fill: '#a4de6c',
//   },
//   {
//     name: '50+', uv: 12, pv: 4800, fill: '#d0ed57',
//   },
 
// ];

const data4 = [
  {
    name: 'Ethnicity', Indian: 0.2, Lankan: 0.7, amt: 9000,
 
  },
]

// const style3 = {
//   top: 0,
//   left: 350,
//   lineHeight: '24px',
// };



function App() {
  return (
  <div className="overAll selec">
     
      <div className="jumbotron shadow-lg" >
            <h1 className="header">Header</h1>
        </div>
{/* container col */}
        <div className="container colx1">
        <div className="row">
            <div className="col" align="center">
               <div className="card text-white bg-primary mb-3 shadow-lg stl1">
                <div class="card-header shadow-lg stl2">Screens</div>
                    <div className="card-body">
                        <h5 className="card-title stl3" >2</h5>
                   </div>
                </div>
            </div>
            <div className="col" align="center">
                   <div className="card text-white bg-primary mb-3 shadow-lg stl4" >
                       <div className="card-header shadow-lg stl5" >Watchers</div>
                    <div className="card-body">
                        <h5 className="card-title stl6" >28</h5>
                </div>
              </div>
             </div>
           </div>
          </div>
{/* end container col */}
    <div className="cont1">
      <div className="container-fluid mont1">
          <p className="mont">Aggregated Data for All Signages</p>
          <p className="mont2">Looking at Individual Signages</p>
      </div>
    </div>

{/* Looking ^ content */}

         <div className="container-fluid rw1">
            <div className="row">
              <div className="col-md-5 id1" align="Center"> Signages ID</div>
              <div className="col-md-5">
                <Select className="sel1 shadow-lg" options={Company} isMulti/>
              </div>
              {/* <div className="col-md-4"></div> */}
            </div>
          </div>

      {/* Sig ID end */}
      
          <div className="container-fluid rw1">
            <div className="row">
              {/* <div className="col-md-5 id1" align="Center"> Signages ID</div> */}
              <div className="col-md-5">
              </div>
              {/* <div className="col-md-4"></div> */}
            </div>
          </div>

{/* Graph Starting */}
      <div className="container-fluid ch2">
        
      <ScatterChart className="sct1"
        width={900}
        height={400}
        margin={{
          top:10, right: 20, bottom: 10, left: 20,
        }}
      >
        <CartesianGrid />
        <XAxis type="number" dataKey="x" name="time" unit=" sec" />
        <YAxis type="number" dataKey="y" name="unit" unit=" t" />
        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
        <Scatter name="Time" data={data} fill="#4235ea" />
      </ScatterChart>

     </div>

        
         <div className="container-fluid rw1">
            <div className="row gen2">
              <div className="col-md-5 id1" align="left"> Gender 
                 <Select className="sel1 shadow-lg gen3" options={Gen} isMulti/>
                <div className="mg2">Age Group <Select className="sel1 shadow-lg gen3" options={Age} isMulti/> </div>
              </div> 

              <div className="col-md-5 id1" align="center">
                  Ethnicity Graph 
                  
                  <BarChart
                      width={230}
                      height={300}
                      data={data4}
                      margin={{
                        top: 20, right: 30, left: 20, bottom: 5,
                      }}
                    >
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Legend />
                      <Bar dataKey="Lankan" stackId="a" fill="#4235ea" />
                      <Bar dataKey="Indian" stackId="a" fill="#35ea74" />
                    </BarChart>

                  {/* <PieChart
                      data={[
                        { title: 'One', value: 10, color: '#E38627', name: 'io' },
                        { title: 'Two', value: 15, color: '#C13C37' },
                        { title: 'Three', value: 20, color: '#6A2135' },
                      ]}
                    />; */}

              </div>
              {/* <div className="col-md-4"></div> */}
            </div>
          </div>


{/*  test */}
          <div className="container-fluid rw1 mg12">
            <div className="row">
              <div className="col-md-5 id1 id12" align="Center"> People's Data</div>
              <div className="col-md-5">
                <Select className="sel1 sel1-1 shadow-lg" options={Details} isMulti/>
              </div>
              {/* <div className="col-md-4"></div> */}
            </div>
          </div>

          <div className="container">
          {/* <RadialBarChart width={500} height={300} cx={150} cy={150} innerRadius={40} outerRadius={160} barSize={60} data={data3}>
        <RadialBar minAngle={15} label={{ position: 'insideStart', fill: '#000' }} background clockWise dataKey="uv" />
        <Legend iconSize={20} width={120} height={140} layout="vertical" verticalAlign="middle" wrapperStyle={style3} />
      </RadialBarChart>

      <Line percent="10" strokeWidth="2" strokeColor="#4235ea" trailWidth="4" /> */}

          </div>


      {/* DATA's */}
          {/* <div className="container-fluid rw1">
            <div className="row">
              <div className="col-md-5 id1" align="Center">
              <Line percent="80" strokeWidth="2" strokeColor="#4235ea" trailWidth="4" />

              </div>
              <div className="col-md-5">
                <h5>70%</h5>
              <CircleProgress percentage={90} strokeWidth={8} secondaryColor="#f0f0f0" />

              </div>
              <div className="col-md-4"> HI</div>
            </div>
          </div>
 */}


    <div className="container-fluid rw1 mr-1">
            <div className="row gen2 ">
              <div className="col-md-5 id1 id16 shadow-lg" align="left"> Sequence Of Emotions
                 {/* <Select className="sel1 shadow-lg gen3" options={Gen} isMulti/> */}
                <div align="left" className="txt-sm ">
                 <Line percent="18" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Normal [ 18% ]</div>
                 <Line percent="10" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Sad [ 10% ]</div>
                 <Line percent="6" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Fear [ 6% ]</div>
                 <Line percent="63" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Exiting [ 73% ]</div>
                </div>
          {/* <div className="mg2">Age Group <Select className="sel1 shadow-lg gen3" options={Age} isMulti/> </div> */}
              </div> 
{/* col start */}
              <div className="col-md-5 id1 col-13 " align="left">
                Aggregated Emotions
              <div className="txt-sm spc-14  shadow-lg"> <CircleProgress percentage={36} strokeWidth={12} secondaryColor="#f0f0f0" width="150" /> Neutral</div>
               
              <div className="txt-sm shadow-lg">  <CircleProgress percentage={73} strokeWidth={12} secondaryColor="#f0f0f0" width="150" /> Happiness </div>
               
              
                  {/* <PieChart
                      data={[
                        { title: 'One', value: 10, color: '#E38627', name: 'io' },
                        { title: 'Two', value: 15, color: '#C13C37' },
                        { title: 'Three', value: 20, color: '#6A2135' },
                      ]}
                    />; */}

              </div>
              {/* <div className="col-md-4"></div> */}
            </div>
          </div>



{/* lst DATA's */}


                      
    <div className="container-fluid rw1 mr-1">
            <div className="row gen2 ">
              <div className="col-md-5 id1 id16 shadow-lg" align="left"> Facial Attributes
                 {/* <Select className="sel1 shadow-lg gen3" options={Gen} isMulti/> */}
                <div align="left" className="txt-sm ">
                 <Line percent="28" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Glasses [ 28%  ]</div>
                 <Line percent="72" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13"trailColor="#f1eded" />
                 <div className="spc-15">Young [ 72% ]</div>
                 <Line percent="37" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Beard [ 37% ]</div>
               
                 <Line percent="28" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Mustache [ 28% ]</div>  <Line percent="38" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded" />
                 <div className="spc-15">Foreigner [ 38% ]</div>  <Line percent="62" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Women [ 62% ]</div>
                 <Line percent="45" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Mens [ 45% ]</div>
                 <Line percent="83" strokeWidth="3" strokeColor="#00BBFF" trailWidth="4" className="spc-13 sp-13" trailColor="#f1eded"/>
                 <div className="spc-15">Using Phone [ 83% ]</div>

                </div>
          {/* <div className="mg2">Age Group <Select className="sel1 shadow-lg gen3" options={Age} isMulti/> </div> */}
              </div> 
{/* col start */}
              <div className="col-md-5 id1 col-13 " align="left">
                Clothing & Other Details
              <div className="txt-sm spc-14  shadow-lg"> <CircleProgress percentage={73} strokeWidth={12} secondaryColor="#f0f0f0" width="120" /> T-Shirt</div>
               
              <div className="txt-sm shadow-lg">  <CircleProgress percentage={53} strokeWidth={12} secondaryColor="#f0f0f0" width="120" /> Jeans</div>
              <div className="txt-sm shadow-lg">  <CircleProgress percentage={32} strokeWidth={12} secondaryColor="#f0f0f0" width="120" /> Carrybags </div>
              <div className="txt-sm shadow-lg">  <CircleProgress percentage={41} strokeWidth={12} secondaryColor="#f0f0f0" width="120" /> Vehicle</div>
              <div className="txt-sm shadow-lg">  <CircleProgress percentage={63} strokeWidth={12} secondaryColor="#f0f0f0" width="120" /> Goggles </div>

              
                  {/* <PieChart
                      data={[
                        { title: 'One', value: 10, color: '#E38627', name: 'io' },
                        { title: 'Two', value: 15, color: '#C13C37' },
                        { title: 'Three', value: 20, color: '#6A2135' },
                      ]}
                    />; */}

              </div>
              {/* <div className="col-md-4"></div> */}
            </div>
          </div>


          {/* <h1 className="header1">LOCAD OOH & DOOH</h1> */}
          <div className="dv1 shadow-lg" >
            <h1 className="header1">Copyright: LOCAD ADTECH FOR OOH & DOOH | 2020</h1>
        </div>

                    {/* <Circle percent="70" strokeColor="#4235ae" strokeWidth="2" /> */}

          {/* <ProgressBar label="Loading..." value={50} />

          <Flex direction="column" gap="size-300">
            <ProgressBar label="Large" size="L" value={80} />
          </Flex> */}
     {/* <div className="fusion2">       */}
     {/*  FUSION CHART  */}
     {/* <ReactFusioncharts
        type="scatter"
        width="60%"
        height="40%"
        dataFormat="JSON"
        dataSource={dataSource}
      /> */}
{/* 
<div className="container-fluid rw1">
            <div className="row">
              <div className="col-md-5 id1" align="Center"> Signages ID GEN <Select className="sel1 shadow-lg" options={Company} isMulti/></div>
              <div className="col-md-5">
                  Side ID 2
              </div>
              {/* <div className="col-md-4"></div> */}
            {/* </div>
          </div> */} 



{/* END MAIN Div */}
  </div>
  );
}

export default App;
